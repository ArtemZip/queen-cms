package com.gitlab.artemzip.queencms.core.processors

import java.lang.reflect.Field

/**
 * Create a bean of this object to bind own type in ConfigValue injection
 *
 * @see ConfigValue
 * @see ConfigValueProcessor
 */
@FunctionalInterface
interface ConfigValueHook {

    /**
     * @return true if value was setted to bean's field
     * 
     * @Attention don't forget to check field type
     */
    fun setField(field: Field, bean: Any, value: String) : Boolean
}
