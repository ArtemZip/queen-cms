package com.gitlab.artemzip.queencms.core.processors

import com.gitlab.artemzip.queencms.core.annotations.ConfigValue
import com.gitlab.artemzip.queencms.data.services.ConfigurationAssetService
import org.springframework.beans.factory.config.BeanPostProcessor
import org.springframework.context.annotation.Configuration
import org.springframework.util.ReflectionUtils
import java.lang.reflect.Field


/**
 * Processor for loading values from db by the help annotation @ConfigValue
 * Works for primitive types and String, if you want to inject your object
 * you can create own ConfigValueHook as bean to do this
 *
 * @see ConfigValue
 * @see ConfigValueHook
 * @see BeanPostProcessor
 */
@Configuration
class ConfigValueProcessor(
    val configService : ConfigurationAssetService,
    val hooks: List<ConfigValueHook>

) : BeanPostProcessor {

    override fun postProcessBeforeInitialization(bean: Any, beanName: String): Any? {
        ReflectionUtils.doWithFields(bean::class.java) {
            enrichField(it, bean)
        }
        return bean
    }

    private fun enrichField(field: Field, bean: Any) {
        if (field.getAnnotation(ConfigValue::class.java) != null) {
            val annotation = field.getAnnotation(ConfigValue::class.java)
            field.trySetAccessible()

            val value = getConfigurationValue(annotation)
            when(field.type) {
                Int::class.java, Integer::class.java -> field.set(bean, value.toInt())
                Byte::class.java, java.lang.Byte::class.java -> field.set(bean, value.toByte())
                Double::class.java, java.lang.Double::class.java -> field.set(bean, value.toDouble())
                Long::class.java, java.lang.Long::class.java -> field.set(bean, value.toLong())
                Boolean::class.java, java.lang.Boolean::class.java -> field.set(bean, value.toBoolean())
                String::class.java -> field.set(bean, value)
                else -> useHooks(field, bean, value)
            }
        }
    }

    private fun useHooks(field: Field, bean: Any, value: String) {
        hooks.takeWhile { !it.setField(field, bean, value) }
    }

    private fun getConfigurationValue(annotation: ConfigValue): String {
        var result = ""
        var assetId = ""
        var configName = ""

        if (annotation.value.isNotEmpty()) {
            val values = annotation.value.split(':')
            if (values.size == 2) {
                assetId = values.first()
                configName = values.last()
            }
        } else {
            assetId = annotation.assetId
            configName = annotation.config
        }

        configService.findById(assetId).ifPresent {
            result = it.configurations.getOrDefault(configName, result)
        }
        return result
    }
}
