package com.gitlab.artemzip.queencms.core.annotations

import org.springframework.boot.autoconfigure.SpringBootApplication
import java.lang.annotation.Inherited

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Inherited
@SpringBootApplication(scanBasePackages = ["com.gitlab.artemzip.queencms"])
annotation class QueenCMS
