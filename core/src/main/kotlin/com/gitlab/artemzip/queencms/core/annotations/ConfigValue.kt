package com.gitlab.artemzip.queencms.core.annotations

import com.gitlab.artemzip.queencms.core.processors.ConfigValueHook
import com.gitlab.artemzip.queencms.core.processors.ConfigValueProcessor

/**
 * Annotation for injecting values from db
 *
 * @see ConfigValueHook
 * @see ConfigValueProcessor
 */

@Target(
    AnnotationTarget.FIELD,
    AnnotationTarget.VALUE_PARAMETER,
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
annotation class ConfigValue(
    val value: String = "",
    val assetId: String = "",
    val config: String = ""
)
