package com.gitlab.artemzip.queencms.core

import com.gitlab.artemzip.queencms.core.annotations.QueenCMS
import org.springframework.boot.runApplication

@QueenCMS
internal class CoreApplication

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<CoreApplication>(*args)
}
