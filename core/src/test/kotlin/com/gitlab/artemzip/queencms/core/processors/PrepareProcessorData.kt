package com.gitlab.artemzip.queencms.core.processors

import com.fasterxml.jackson.databind.ObjectMapper
import com.gitlab.artemzip.queencms.data.assets.ConfigurationAsset
import com.gitlab.artemzip.queencms.data.services.ConfigurationAssetService
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.lang.reflect.Field

@Configuration
class PrepareProcessorData(val configService: ConfigurationAssetService) : ApplicationRunner {
    companion object {
        const val FIRST_TEST_ID = "first_test_id"
        const val SECOND_TEST_ID = "second_test_id"
        const val THIRD_TEST_ID = "third_test_id"
    }

    override fun run(args: ApplicationArguments?) {
        configService.saveAll(
            mutableListOf(
                ConfigurationAsset(
                    id = FIRST_TEST_ID, name = FIRST_TEST_ID, configurations = mutableMapOf(
                        Pair("int", "10"),
                        Pair("double", "10.5"),
                        Pair("string", "test")
                    )
                ),
                ConfigurationAsset(
                    id = SECOND_TEST_ID, name = SECOND_TEST_ID, configurations = mutableMapOf(
                        Pair("byte", 2.toByte().toString()),
                        Pair("long", "111111"),
                        Pair("bool", "true")
                    )
                ),
                ConfigurationAsset(id = THIRD_TEST_ID, name = THIRD_TEST_ID, configurations = mutableMapOf(
                        Pair("object", ObjectMapper().writeValueAsString(SimpleObj("test", 22))),
                        Pair("unknown", UnknownObj("test").toString())
                    )
                )
            )
        )
    }

    @Bean
    fun simpleHook() = object : ConfigValueHook {
        override fun setField(field: Field, bean: Any, value: String): Boolean {
            if(field.type.equals(SimpleObj::class) || field.type.equals(SimpleObj::class.java)) {
                field.set(bean, ObjectMapper().readValue(value, SimpleObj::class.java))
                return true
            }
            return false
        }
    }

    data class SimpleObj (
        val first: String,
        val second: Int
    ) {
        constructor() : this("", 0)
    }

    data class UnknownObj(val test: String)
}
