package com.gitlab.artemzip.queencms.core

import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
abstract class AbstractTest
