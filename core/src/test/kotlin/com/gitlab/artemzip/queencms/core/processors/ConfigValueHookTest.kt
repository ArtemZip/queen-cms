package com.gitlab.artemzip.queencms.core.processors

import com.gitlab.artemzip.queencms.core.AbstractTest
import com.gitlab.artemzip.queencms.core.annotations.ConfigValue
import com.gitlab.artemzip.queencms.core.processors.PrepareProcessorData.Companion.THIRD_TEST_ID
import com.gitlab.artemzip.queencms.core.processors.PrepareProcessorData.SimpleObj
import com.gitlab.artemzip.queencms.core.processors.PrepareProcessorData.UnknownObj
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ConfigValueHookTest : AbstractTest() {

    @ConfigValue("$THIRD_TEST_ID:object")
    val testObj: SimpleObj? = null

    @ConfigValue("$THIRD_TEST_ID:unknown")
    val testUnknownObj: UnknownObj? = null

    @Test
    fun `Check if custom hook works`() {
        assertThat(testObj)
            .hasFieldOrPropertyWithValue("first", "test")
            .hasFieldOrPropertyWithValue("second", 22)
    }

    @Test
    fun `Object is not found if hook doesn't exist`() {
        assertThat(testUnknownObj).isNull()
    }
}
