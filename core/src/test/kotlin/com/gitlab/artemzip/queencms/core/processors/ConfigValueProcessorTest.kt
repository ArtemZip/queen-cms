package com.gitlab.artemzip.queencms.core.processors

import com.gitlab.artemzip.queencms.core.AbstractTest
import com.gitlab.artemzip.queencms.core.annotations.ConfigValue
import com.gitlab.artemzip.queencms.core.processors.PrepareProcessorData.Companion.FIRST_TEST_ID
import com.gitlab.artemzip.queencms.core.processors.PrepareProcessorData.Companion.SECOND_TEST_ID
import com.gitlab.artemzip.queencms.core.processors.PrepareProcessorData.Companion.THIRD_TEST_ID
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class ConfigValueProcessorTest : AbstractTest() {

    @ConfigValue(assetId = FIRST_TEST_ID, config = "int")
    var testInteger: Int? = null

    @ConfigValue(assetId = FIRST_TEST_ID, config = "double")
    var testDouble: Double? = null

    @ConfigValue(assetId = FIRST_TEST_ID, config = "string")
    var testString: String? = null

    @ConfigValue(assetId = SECOND_TEST_ID, config = "byte")
    var testByte: Byte? = null

    @ConfigValue(assetId = SECOND_TEST_ID, config = "long")
    var testLong: Long? = null

    @ConfigValue(assetId = SECOND_TEST_ID, config = "bool")
    var testBool: Boolean? = null

    @ConfigValue(assetId = THIRD_TEST_ID, config = "unexpected")
    var testNull: String? = null

    @ConfigValue("$FIRST_TEST_ID:int")
    var shortDescTestInt : Int? = null

    @ConfigValue("$FIRST_TEST_ID:double")
    var shortDescTestDouble: Double? = null

    @ConfigValue("$FIRST_TEST_ID:string")
    var shortDescTestString: String? = null

    @ConfigValue("$SECOND_TEST_ID:byte")
    var shortDescTestByte: Byte? = null

    @ConfigValue("$SECOND_TEST_ID:long")
    var shortDescTestLong: Long? = null

    @ConfigValue("$SECOND_TEST_ID:bool")
    var shortDescTestBool: Boolean? = null

    @ConfigValue("$THIRD_TEST_ID:unexpected")
    var shortDescTestNull: String? = null

    @Test
    fun `Check of setting class fields by @ConfigValue (long desc)`() {
        assertThat(testInteger).isEqualTo(10)
        assertThat(testDouble).isEqualTo(10.5)
        assertThat(testString).isEqualTo(testString)
        assertThat(testByte).isEqualTo(2.toByte())
        assertThat(testLong).isEqualTo(111111L)
        assertThat(testBool).isEqualTo(true)
        assertThat(testNull).isNullOrEmpty()
    }

    @Test
    fun `Check of setting class fields by @ConfigValue (short desc)` () {
        assertThat(shortDescTestInt).isEqualTo(10)
        assertThat(shortDescTestDouble).isEqualTo(10.5)
        assertThat(shortDescTestString).isEqualTo(testString)
        assertThat(shortDescTestByte).isEqualTo(2.toByte())
        assertThat(shortDescTestLong).isEqualTo(111111L)
        assertThat(shortDescTestBool).isEqualTo(true)
        assertThat(shortDescTestNull).isNullOrEmpty()
    }

}


