package com.gitlab.artemzip.queencms.data

import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
abstract class AbstractDataTest
