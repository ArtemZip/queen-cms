package com.gitlab.artemzip.queencms.data.configurations

import com.gitlab.artemzip.queencms.data.AbstractDataTest
import com.gitlab.artemzip.queencms.data.ROOT_CONFIGURATION_ID
import com.gitlab.artemzip.queencms.data.ROOT_FOLDER_ID
import com.gitlab.artemzip.queencms.data.services.ConfigurationAssetService
import com.gitlab.artemzip.queencms.data.services.FolderAssetService
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class DataConfigurationTest : AbstractDataTest() {

    @Autowired
    var folderAssetService: FolderAssetService? = null

    @Autowired
    var configurationAssetService: ConfigurationAssetService? = null

    @Test
    fun `Check whether ROOT folder was correctly created`() {
        assertThat(folderAssetService!!.findById(ROOT_FOLDER_ID))
            .isPresent.get()
            .matches({
                it.getChildren().contains(ROOT_CONFIGURATION_ID)
            }, "Root folder must contains root configs")
    }

    @Test
    fun `Check whether ROOT config was correctly created`() {
        assertThat(configurationAssetService!!.findById(ROOT_CONFIGURATION_ID))
            .isPresent.get()
            .extracting { it.parentId }
            .isEqualTo(ROOT_FOLDER_ID)
    }
}
