package com.gitlab.artemzip.queencms.data.services

import com.gitlab.artemzip.queencms.data.AbstractDataTest
import com.gitlab.artemzip.queencms.data.assets.ConfigurationAsset
import com.gitlab.artemzip.queencms.data.assets.FolderAsset
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired

class AssetServiceTest : AbstractDataTest() {

    @Autowired
    var folderAssetService: FolderAssetService? = null

    @Autowired
    var configurationAssetService: ConfigurationAssetService? = null

    @Test
    fun `Folder data test`(){
        val asset = folderAssetService!!.save(FolderAsset("FolderSaveTest", "FolderSaveTest"))
        assertThat(folderAssetService!!.findById(asset.parentId))
            .get().matches{ it.getChildren().contains("FolderSaveTest") }

        folderAssetService!!.delete(asset)
        assertThat(folderAssetService!!.findById(asset.id)).isNotPresent
        assertThat(folderAssetService!!.findById(asset.parentId))
            .get().matches{ !it.getChildren().contains("FolderSaveTest") }
    }

    @Test
    fun `Configuration data test`(){
        val asset = configurationAssetService!!.save(ConfigurationAsset("ConfSaveTest", "ConfSaveTest"))
        assertThat(folderAssetService!!.findById(asset.parentId))
            .get().matches{ it.getChildren().contains("ConfSaveTest") }

        configurationAssetService!!.delete(asset)
        assertThat(configurationAssetService!!.findById(asset.id)).isNotPresent
        assertThat(folderAssetService!!.findById(asset.parentId))
            .get().matches{ !it.getChildren().contains("ConfSaveTest") }
    }

    @Test
    fun `Create folder with content and fail on deletion`() {
        var folderAsset = folderAssetService!!.save(
            FolderAsset(id = "FolderWithContentTest", name = "FolderWithContentTest")
        )
        val confAsset = configurationAssetService!!.save(
            ConfigurationAsset(id = "ConfSaveTest", name = "ConfSaveTest", parentId = folderAsset.id)
        )
        folderAsset = folderAssetService!!.findById(folderAsset.id).get()
        assertThat(folderAsset).matches { it.getChildren().contains(confAsset.id) }

        assertThrows<IllegalArgumentException>("Asset#FolderWithContentTest is not empty Folder") {
            folderAssetService!!.delete(folderAsset)
        }

        configurationAssetService!!.delete(confAsset)
        folderAssetService!!.delete(folderAssetService!!.findById(folderAsset.id).get())

        assertThat(folderAssetService!!.findById(folderAsset.parentId))
            .get().matches{ !it.getChildren().contains("FolderWithContentTest") }
    }
}
