package com.gitlab.artemzip.queencms.data.repositories

import com.gitlab.artemzip.queencms.data.assets.ConfigurationAsset
import org.springframework.data.mongodb.repository.MongoRepository

internal interface ConfigurationAssetRepository : MongoRepository<ConfigurationAsset, String>
