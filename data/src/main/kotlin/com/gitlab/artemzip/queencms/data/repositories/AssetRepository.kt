package com.gitlab.artemzip.queencms.data.repositories

import com.gitlab.artemzip.queencms.data.assets.Asset
import org.springframework.data.mongodb.repository.MongoRepository

interface AssetRepository : MongoRepository<Asset, String>
