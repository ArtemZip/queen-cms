package com.gitlab.artemzip.queencms.data.assets

import com.gitlab.artemzip.queencms.data.ROOT_FOLDER_ID
import org.springframework.data.annotation.Id

open class Asset(
    @Id
    open val id: String,
    open val name: String = id,
    open val parentId: String = ROOT_FOLDER_ID

) {

    override fun toString(): String {
        return "Asset(id='$id', name='$name', parentId='$parentId')"
    }
}

