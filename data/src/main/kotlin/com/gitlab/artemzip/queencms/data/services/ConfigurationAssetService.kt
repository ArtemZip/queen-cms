package com.gitlab.artemzip.queencms.data.services

import com.gitlab.artemzip.queencms.data.assets.ConfigurationAsset
import com.gitlab.artemzip.queencms.data.repositories.FolderAssetRepository
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Service

@Service
class ConfigurationAssetService(
    folderAssetRepository: FolderAssetRepository,
    assetRepository: MongoRepository<ConfigurationAsset, String>
) : AbstractAssetService<ConfigurationAsset>(assetRepository, folderAssetRepository)
