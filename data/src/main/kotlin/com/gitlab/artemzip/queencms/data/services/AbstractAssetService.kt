package com.gitlab.artemzip.queencms.data.services

import com.gitlab.artemzip.queencms.data.assets.Asset
import com.gitlab.artemzip.queencms.data.assets.FolderAsset
import com.gitlab.artemzip.queencms.data.repositories.FolderAssetRepository
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.util.Assert


abstract class AbstractAssetService<T : Asset> (
    private val assetRepository: MongoRepository<T, String>,
    private val folderAssetRepository: FolderAssetRepository

) : MongoRepository<T, String> by assetRepository {

    override fun <S : T> save(asset: S): S {
        val parent = folderAssetRepository.findById(asset.parentId)

        Assert.isTrue(parent.isPresent, "Asset's parent with id ${asset.parentId} doesn't exist")
        Assert.isInstanceOf(
            FolderAsset::class.java,
            parent.get(),
            "Asset's parent must be of type ${FolderAsset::class}, but it is ${parent.get().javaClass}"
        )

        val parentFolder: FolderAsset = parent.get()
        parentFolder.addChild(asset)
        folderAssetRepository.save(parentFolder)
        return assetRepository.save(asset)
    }

    override fun <S : T> saveAll(entities: MutableIterable<S>): MutableList<S> = entities.map{
        save(it)
    }.toMutableList()

    override fun deleteById(id: String) {
        val optional = assetRepository.findById(id)
        if(optional.isPresent) {
            delete(optional.get())
        }
    }

    override fun delete(asset: T) {
        if(asset is FolderAsset) {
            Assert.isTrue(
                (asset as FolderAsset).getChildren().isEmpty(),
            "Asset#${asset.id} is not empty Folder"
            )
        }

        val parent = folderAssetRepository.findById(asset.parentId).get()
        parent.removeChild(asset)
        folderAssetRepository.save(parent)
        assetRepository.delete(asset)
    }

    override fun deleteAll(entities: MutableIterable<T>) = entities.forEach { delete(it) }
}
