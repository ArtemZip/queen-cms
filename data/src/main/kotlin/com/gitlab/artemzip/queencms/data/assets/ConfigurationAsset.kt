package com.gitlab.artemzip.queencms.data.assets

import com.gitlab.artemzip.queencms.data.ROOT_FOLDER_ID

open class ConfigurationAsset(
    id: String,
    name: String,
    parentId: String = ROOT_FOLDER_ID,
    open val configurations: MutableMap<String, String> = mutableMapOf()

) : Asset(id, name, parentId) {

    override fun toString(): String {
        return "ConfigurationAsset(id='$id', name='$name', parentId='$parentId', configurations=$configurations)"
    }
}
