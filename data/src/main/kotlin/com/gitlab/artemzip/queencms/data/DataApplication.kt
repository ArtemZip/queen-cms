package com.gitlab.artemzip.queencms.data

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["com.gitlab.artemzip.queencms"])
internal class DataApplication

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<DataApplication>(*args)
}
