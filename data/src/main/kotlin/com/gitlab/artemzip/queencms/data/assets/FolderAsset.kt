package com.gitlab.artemzip.queencms.data.assets

import com.gitlab.artemzip.queencms.data.ROOT_FOLDER_ID
import org.springframework.data.mongodb.core.mapping.Document

@Document
open class FolderAsset (
    id: String,
    name: String,
    parentId: String = ROOT_FOLDER_ID,
    private val children: MutableSet<String> = mutableSetOf()

) : Asset(id, name, parentId) {

    override fun toString(): String {
        return "FolderAsset(id='$id', name='$name', parentId='$parentId', children=${children})"
    }

    fun addChild(assetId: String) = children.add(assetId)

    fun addChild(asset: Asset) = children.add(asset.id)

    fun removeChild(assetId: String) = children.remove(assetId)

    fun removeChild(asset: Asset) = children.remove(asset.id)

    fun getChildren() = children
}
