package com.gitlab.artemzip.queencms.data.configurations

import com.gitlab.artemzip.queencms.data.ROOT_CONFIGURATION_ID
import com.gitlab.artemzip.queencms.data.ROOT_FOLDER_ID
import com.gitlab.artemzip.queencms.data.assets.ConfigurationAsset
import com.gitlab.artemzip.queencms.data.assets.FolderAsset
import com.gitlab.artemzip.queencms.data.repositories.FolderAssetRepository
import com.gitlab.artemzip.queencms.data.services.ConfigurationAssetService
import org.springframework.context.annotation.Configuration

@Configuration
class DataConfiguration(folderAssetRepository: FolderAssetRepository, configService: ConfigurationAssetService) {

    init {
        folderAssetRepository.save(FolderAsset(ROOT_FOLDER_ID, ROOT_FOLDER_ID))
        configService.save(ConfigurationAsset(ROOT_CONFIGURATION_ID, ROOT_FOLDER_ID))
    }
}
