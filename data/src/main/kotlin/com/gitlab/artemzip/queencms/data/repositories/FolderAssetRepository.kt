package com.gitlab.artemzip.queencms.data.repositories

import com.gitlab.artemzip.queencms.data.assets.FolderAsset
import org.springframework.data.mongodb.repository.MongoRepository

interface FolderAssetRepository : MongoRepository<FolderAsset, String>
