package com.gitlab.artemzip.queencms.data.services

import com.gitlab.artemzip.queencms.data.assets.FolderAsset
import com.gitlab.artemzip.queencms.data.repositories.FolderAssetRepository
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Service

@Service
class FolderAssetService(
    folderAssetRepository: FolderAssetRepository,
    assetRepository: MongoRepository<FolderAsset, String>
) : AbstractAssetService<FolderAsset>(assetRepository, folderAssetRepository)
