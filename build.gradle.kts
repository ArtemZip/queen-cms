import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

var projectGroup = "com.gitlab.artemzip.queencms"
var projectVersion = "0.0.1-SNAPSHOT"

plugins {
    kotlin("jvm") version "1.3.70" apply false
    kotlin("plugin.spring") version "1.3.70" apply false
    id("org.springframework.boot") version "2.2.6.RELEASE" apply false
    id("io.spring.dependency-management") version "1.0.9.RELEASE" apply false
    id("io.gitlab.arturbosch.detekt") version("1.10.0") apply true
}

allprojects {
    group = projectGroup
    version = projectVersion

    repositories {
        jcenter()
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "1.8"
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }

}

subprojects {
    group = projectGroup
    version = projectVersion
}

val detektAll by tasks.registering(io.gitlab.arturbosch.detekt.Detekt::class) {
    description = "Static code analyzing for whole project"
    parallel = true
    buildUponDefaultConfig = true
    setSource(projectDir)
    include("**/*.kt", "**/*.kts")
    exclude( "**/resources/**", "**/build/**")
    reports {
        xml.enabled = false
        html.enabled = false
        txt.enabled = false
    }
}
