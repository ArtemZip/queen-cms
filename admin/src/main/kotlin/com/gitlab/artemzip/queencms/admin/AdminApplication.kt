package com.gitlab.artemzip.queencms.admin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
internal class AdminApplication

@Suppress("SpreadOperator")
fun main(args: Array<String>) {
    runApplication<AdminApplication>(*args)
}
