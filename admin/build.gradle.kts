
plugins {
    id("org.springframework.boot")
    id("io.spring.dependency-management")
    kotlin("jvm")
    kotlin("plugin.spring")
}

java.sourceCompatibility = JavaVersion.VERSION_11

dependencies {
    implementation(project(":data"))
    implementation(kotlin("stdlib"))
    implementation("org.springframework.boot:spring-boot-starter")
}
